# zsh

Z shell configuration file.

## Requirements
* [Powerlevel10k](https://github.com/romkatv/powerlevel10k)

## Installation
1. Install Powerlevel10k using the manual. Note if installing manually, it might
   be necessary to switch to Zsh as Bash will not interpret `>>!` correctly.
2. Configure Powerlevel10k using the configuration wizard.
3. Symlink the zshrc `ln -s $PWD/zshrc ~/.zshrc`.
4. Symlink the Powerlevel10k git repo to `ln -s <Powerlevel10k GIT REPO>
   ~/.config/powerlevel10k`.
